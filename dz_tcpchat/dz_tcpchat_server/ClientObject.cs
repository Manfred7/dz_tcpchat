﻿using System;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using dz_tcpchat_lib;

namespace dz_tcpchat_server
{
    public class ClientObject
    {
        TcpClient client;
        ServerObject server; // объект сервера
        string userName;

        public ClientObject(TcpClient tcpClient, ServerObject serverObject)
        {
            Id = Guid.NewGuid().ToString();
            client = tcpClient;
            server = serverObject;
            serverObject.AddConnection(this);
        }

        protected internal string Id { get; private set; }
        protected internal NetworkStream Stream { get; private set; }

        private void OnLeavChat()
        {
            ChatMessage myMsg = new ChatMessage();
            myMsg.MessageInit(userName,String.Format($"{userName}: покинул чат"));
            Console.WriteLine(myMsg.userMessage);

            server.BroadcastMessage(myMsg, this.Id);
        }

        private void RunMessageLoop()
        {
            ChatMessage message;

            // в бесконечном цикле получаем сообщения от клиента
            while (true)
            {
                try
                {
                    message = GetMessage();
                    Console.WriteLine(message.ToString());
                    server.BroadcastMessage(message, this.Id);
                }
                catch
                {
                    OnLeavChat();
                    break;
                }
            }
        }

        private void ConnectNewUser()
        {
            Stream = client.GetStream();
            ChatMessage message;
            
            message = GetMessage();
            userName = message.userName;
            string infoMsg = $"{userName}  вошел в чат";
            message.userMessage = infoMsg;

            server.BroadcastMessage(message, this.Id); // посылаем сообщение о входе в чат всем подключенным пользователям
            Console.WriteLine(infoMsg);
        }

        public void Process()
        {
            try
            {
                ConnectNewUser();
                RunMessageLoop();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // в случае выхода из цикла закрываем ресурсы
                server.RemoveConnection(this.Id);
                Close();
            }
        }

        // чтение входящего сообщения 
        private ChatMessage GetMessage()
        {
            var formatter = new BinaryFormatter();
            try
            {
                return (ChatMessage) formatter.Deserialize(Stream);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        // закрытие подключения
        protected internal void Close()
        {
            if (Stream != null)
                Stream.Close();
            if (client != null)
                client.Close();
        }
    }
}