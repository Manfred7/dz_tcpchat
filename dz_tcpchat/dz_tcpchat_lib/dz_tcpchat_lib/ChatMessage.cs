﻿using System;

namespace dz_tcpchat_lib
{
    [Serializable]
    public class ChatMessage
    {
        public string userName { get; set; }
        public DateTime mesTime { get; set; }
        public string userMessage { get; set; }

        public void MessageInit(string uName, string uMess)
        {
            userName = uName;
            mesTime = DateTime.Now;
            userMessage = uMess;
        }

        public override string ToString()
        {
            return String.Format("{0}: {1} : {2}", mesTime.ToShortTimeString(), userName, userMessage);
        }
    }
}