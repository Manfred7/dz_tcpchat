﻿using System;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using dz_tcpchat_lib;

namespace dz_tcpchat_client
{
    public class ChatClient
    {
        string userName;
        private  string serverHost;
        private  int serverPort;
        private TcpClient netClient;
        private  NetworkStream netStream;
       
        public void InitConfiguration(string host, int port)
        {
            serverHost = host;   
            serverPort = port;
        }

        private void ChatLogin()
        {
            Console.Write("Введите свое имя: ");
            userName = Console.ReadLine();
            //SendUserConnectCommand();
            SendUserConnectMessage();
        }

        private void StartReceiveMessageLoop()
        {
            // запускаем новый поток для получения данных
            Thread receiveThread = new Thread(new ThreadStart(ReceiveMessage));
            receiveThread.Start(); //старт потока   
        }

        private void ServerConnect()
        {
            netClient.Connect(serverHost, serverPort); //подключение клиента
            netStream = netClient.GetStream(); // получаем поток 
        }
        public void RunChat()
        {
            
            netClient = new TcpClient();
            try
            {
                ServerConnect();
                ChatLogin();
                StartReceiveMessageLoop();
                StartSendMessageLoop();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Disconnect();
            }
        }
         private void SendUserConnectMessage()
        {
            ChatMessage myMsg = new ChatMessage();
            myMsg.MessageInit(userName,"");
            
            var formatter = new BinaryFormatter();
            formatter.Serialize(netStream, myMsg);
        }
      

        private void StartSendMessageLoop()
        {
            ChatMessage myMsg = new ChatMessage();
            var formatter = new BinaryFormatter();
            string mes= String.Empty;
            
            Console.WriteLine($"Добро пожаловать, {userName}");
            Console.WriteLine("Введите сообщение: ");
            
            while (true)
            {
                mes = Console.ReadLine();
                myMsg.MessageInit(userName, mes);
                formatter.Serialize(netStream, myMsg);
            }
        }
        // получение сообщений
        private void ReceiveMessage()
        {
            ChatMessage myMsg ;
            string message;
            var formatter = new BinaryFormatter();

            while (true)
            {
                try
                {

                    StringBuilder builder = new StringBuilder();            
                    myMsg= (ChatMessage)formatter.Deserialize(netStream);
                    builder.Append(myMsg.ToString());
                    message = builder.ToString();
                    Console.WriteLine(message); 
                }
                catch
                {
                    Console.WriteLine("Подключение прервано!"); 
                    Console.ReadLine();
                    Disconnect();
                }
            }
        }

        private void Disconnect()
        {
            if (netStream != null)
                netStream.Close(); //отключение потока
            if (netClient != null)
                netClient.Close(); //отключение клиента
            Environment.Exit(0); //завершение процесса
        }
    }
}