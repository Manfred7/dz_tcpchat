﻿using System.Configuration;
namespace dz_tcpchat_client
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var host = ConfigurationManager.AppSettings["ServerHost"];
            var port   = int.Parse(ConfigurationManager.AppSettings["ServerPort"]); 
            
            ChatClient chatClient = new ChatClient();
            chatClient.InitConfiguration(host, port);
            chatClient.RunChat();
        }
    }
}